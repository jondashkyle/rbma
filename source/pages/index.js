var $ = require('cash-dom')
var Handlebars = require('handlebars')
var scrollTop = require('scrolltop')
var mbl = require('mbl')
var Velocity = require('velocity-animate')
var embed = require('embed-video')
var inject = require('svg-inject')
var bowser = require('bowser')
var EventEmitter = require('events').EventEmitter
var fs = require('fs')
var path = require('path')
var template = Handlebars.compile(fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8'))

var _ = {
  each: require('lodash/collection/each'),
  shuffle: require('lodash/collection/shuffle'),
  debounce: require('lodash/function/debounce')
}

function getDocHeight() {
  var D = document
  return Math.max(
    D.body.scrollHeight, D.documentElement.scrollHeight,
    D.body.offsetHeight, D.documentElement.offsetHeight,
    D.body.clientHeight, D.documentElement.clientHeight
  )
}

exports.setup = function setup (content) {
  if (typeof content !== 'object') {
    return false
  }

  var events = new EventEmitter()

  var $el = {
    page: null,
    title: null
  }

  var data = {
    bottom: false
  }

  var title = {
    active: true,
    show: function() {
      if (! title.active) {
        $el.title.css('display', 'block')
        title.active = true
      }
    },
    hide: function() {
      if (title.active && scrollTop() > $el.title.height() / 2) {
        $el.title.css('display', 'none')
        title.active = false
      }
    }
  }

  function on (ev, cb) {
    events.on(ev, cb)
    return this
  }

  function off (ev, cb) {
    events.removeListener(ev, cb)
    return this
  }

  function scroll () {
    title.show()
    clearTimeout(title.debounce)
    title.debounce = setTimeout(title.hide, 500)

    if (! data.bottom && scrollTop() >= getDocHeight() - (window.innerHeight + 10)) {
      data.bottom = true
      events.emit('end')
    } else if (data.bottom && scrollTop() < getDocHeight() - (window.innerHeight + 10)) {
      data.bottom = false
      events.emit('end:up')
    }
  }

  var videos = {
    events : function() {
      $el.page.find('a[href*="youtu.be"]').on('click', videos.toggle)
    },
    toggle : function(e) {
      e.preventDefault()
      if ($(this).is('[data-video-loaded]')) {
        videos.remove($(this))
      } else {
        videos.insert($(this))
      }
    },
    insert : function($link) {
      var id = $link.text().replace(/ /g,'') + Date.now()
      var $videoContain = $('<div data-video-contain="' + id + '" />')
      var $videoAspect  = $('<div data-aspect="4:3" />')
      
      $videoAspect.append(embed($link.attr('href'), { query: { autoplay: 1 } }))
      $videoContain.append($videoAspect)

      if ($link.parents('.unit').length) {
        $videoContain.insertAfter($link.parents('.unit'))  
      } else {
        $videoContain.insertAfter($link)
      }

      var $video = $('[data-video-contain="' + id + '"]')
      Velocity($video, 'scroll', {
        offset: -1 * (window.innerHeight / 2) + ($video.height() / 3)
      })

      $link.attr('data-video-loaded', id)
    },
    remove : function($link) {
      var id = $link.attr('data-video-loaded')
      $link.removeAttr('data-video-loaded')
      $('[data-video-contain="' + id + '"]').remove()
    },
    destroyAll : function() {
      $el.page.find('a[href*="youtu.be"]').each(function() {
        $(this).removeAttr('data-video-loaded')
      })
      $el.page.find('a[href*="youtu.be"]').off('click', videos.toggle)
      $('[data-video-contain]').remove()
    }
  }

  /**
   * Load
   */
  function load (page) {
    $el.page = $('[data-pages] > [data-route="' + page + '"]')
   
    $el.title = $el.page.find('[data-title]')
    $el.title.css('display', 'block')

    if (!bowser.browser.msie) {
      var svg = $el.title.find('img[src$=".svg"]')
      if (svg.length) {
        inject(svg[0])
      }
    }
    
    $('[data-pages]').css('display', 'block')
    $('[data-pages] > [data-route]').css('display', 'none')
    $el.page.css('display', 'block')

    var images = mbl($el.page.find('[data-src]'))
    images.start()

    $('[data-youtube]').each(function(self) {
      $(self).append(embed.youtube($(self).attr('data-youtube')))
    })

    videos.events()

    window.addEventListener('scroll', scroll, false)
  }

  /**
   * Unload
   */
  function unload (page) {
    $('[data-pages]').css('display', 'none')
    $('[data-pages] > [data-route]').css('display', 'none')
    $('iframe').remove()
    videos.destroyAll()
    window.removeEventListener('scroll', scroll, false)
  }

  /**
   * Initialize
   */

  var structure = template(content)
  $('body').append(structure)

  return {
    load: load,
    unload: unload,
    on: on,
    off: off
  }
}