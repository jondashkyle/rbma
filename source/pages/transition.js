var $ = require('cash-dom')
var Velocity = require('velocity-animate')
var _ = {
  pluck : require('lodash/collection/pluck'),
  filter : require('lodash/collection/filter')
}

exports.wipe = function(path, content, callback) {

  // grab title
  var text = _.pluck(_.filter(content, { 'path' : path.replace(/\//g,'') }), 'name')

  // add to dom
  $('body').append('<div data-wipe class="type-heading"><span>' + text + '</span></div>')

  // animate
  Velocity($('[data-wipe] span'), {
    translateX: [($('[data-wipe]').width() + window.innerWidth) * -1, window.innerWidth],
    translateZ: [0, 0]
  }, {
    duration : 800,
    easing: 'linear',
    complete : function() {
      $('[data-wipe]').remove()
      if (callback) callback()
    }
  })

}