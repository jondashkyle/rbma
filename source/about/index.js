var $ = require('cash-dom')
var fs = require('fs')
var path = require('path')
var mobile = require('is-mobile')
var velocity = require('velocity-animate')
var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')
var textFit = require('textfit')

var _ = {
  each: require('lodash/collection/each'),
  shuffle: require('lodash/collection/shuffle')
}

exports.setup = function setup (content) {
  if (typeof content !== 'object') {
    return false
  }

  var data = {
    active: false,
  }

  $('body').append(template)

  function show () {
    if (! data.active) {
      velocity($('[data-about]'), {
        translateY: ['0px', '100%']
      }, {
        duration: 500,
        easing: 'easeOutQuint',
        display: 'block',
        before : function() {
          textResize()
        }
      })
      data.active = true
    }
    if (mobile(navigator.userAgent)) {
      $('.page-hide-nav').css('display', 'block')
    }
  }

  function hide () {
    if (data.active) {
      velocity($('[data-about]'), {
        translateY: ['100%', '0px']
      }, {
        duration: 250,
        easing: 'easeOutQuint',
        display: 'block'
      })
      data.active = false
    }
    if (mobile(navigator.userAgent)) {
      $('.page-hide-nav').css('display', 'none')
    }
  }

  function textResize () {
    if (window.innerWidth > 800) {
      $('[data-credit]').css({
        width : ((window.innerWidth / 12) * 2) + 'px',
        height : '150px'
      })
      setTimeout(function() {
        textFit($('[data-credit]')[0], {
          maxFontSize: 500,
          multiLine: true
        })
        $('[data-about-text]').css('font-size', $('[data-credit]').find('.textFitted').css('font-size'))
      }, 0)
    } else {
      $('[data-credit]').css({
        'width' : 'auto',
        'height' : 'auto'
      })
    }
  }

  $('body').on('fontready', textResize)
  textResize()

  function toggle () {
    if (data.active) {
      hide()
    } else {
      show()
    }
  }

  $('body')
    .on('click', '[data-navigation-hide]', hide)
    .on('touchstart', '[data-navigation-hide]', hide)
    .on('click', '[data-about-toggle]', toggle)

  $('[data-about]')[0].addEventListener('mouseleave', hide, false)

  window.addEventListener('resize', textResize)
  
  return {
    show: show,
    hide: hide,
    toggle: toggle
  }
}