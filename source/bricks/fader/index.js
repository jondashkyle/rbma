var $ = require('cash-dom')
var fs = require('fs')
var path = require('path')
var restyle = require('restyle')
var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')
var helpers = require('../helpers')

var _ = {
  each : require('lodash/collection/each'),
  isEmpty : require('lodash/lang/isEmpty')
}

restyle.customElement(
  'fad-er',
  HTMLElement,
  {
    css : {
      'fad-er': {
        display: 'block',
        width: '100%',
        height: '100%',
        position: 'relative'
      },
      '[data-img]' : {
        position: 'absolute',
        width : '100%',
        height : '100%',
        top : 0,
        left : 0,
        background : {
          size : 'cover',
          position : 'center',
          repeat : 'no-repeat'
        }
      },
      '[data-img]:nth-child(2)' : {
        opacity : '0.5'
      }
    },

    createdCallback : function() {

      // Set up element data
      this.data = {
        speed : 2000,
        playing : false
      }

    },

    detachedCallback : function() {
      // this.css.remove()
    },

    fade : function(element, out) {
      var self = this
      
      var opacity = Math.random() * (0.9 - 0.25) + 0.25;
      
      Velocity(element, {
        opacity : opacity
      }, {
        delay : 0,
        duration : self.data.speed,
        complete : function() {
          self.fade(element)
        }
      })

    },

    pause : function() {
      var self = this
      Velocity(self.querySelectorAll('[data-img]'), 'stop')
      self.data.playing = false
    },

    play : function() {
      var self = this
      if (!self.data.playing) {
        self.fade(self.querySelector('[data-img]:nth-child(1)'))
        self.fade(self.querySelector('[data-img]:nth-child(2)'))
        self.data.playing = true
      }
    },

    onEvents : function(self) {
      self.addEventListener('mouseenter', self.pause)
      self.addEventListener('mouseleave', self.play)
    },

    offEvents : function(self) {
      self.removeEventListener('mouseenter', self.pause)
      self.removeEventListener('mouseleave', self.play)
    },

    /**
     * Public Methods
     */

    start : function() {
      var self = this
      self.play()
      // self.onEvents(self)
    },

    stop : function() {
      var self = this
      self.pause()
      // self.offEvents(self)
    }

  }
)

/**
 * Template Setup
 */
exports.template = function templateGet (self) {
  
  var $container = $('<div/>').html(template)
  
  var images = helpers.randomPhotos(self.images, 2)
  var albums = helpers.filteredAssets(images, 'album_')

  _.each(images, function(image, index) {
    $container.find('fad-er').append('<div data-img style="background-image:url(\'' + image.path + '\');"></div>')
    if (index === 0) {
      // if no album in set, set aspect ratio to first image
      if (_.isEmpty(albums)) {
        $container.find('fad-er')
          .attr('width', image.width)
          .attr('height', image.height)
      } else {
        // otherwise set to square
        $container.find('fad-er')
          .attr('width', 500)
          .attr('height', 500)
      }
    }
  })
  
  return $container[0].innerHTML
}