var fs = require('fs')
var $ = require('cash-dom')
var path = require('path')
var restyle = require('restyle')
var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')
var next = require('array-next')
var helpers = require('../helpers')

var _ = {
  each : require('lodash/collection/each'),
  isEmpty : require('lodash/lang/isEmpty')
}

restyle.customElement(
  'cycl-er',
  HTMLElement,
  {
    css : {
      'cycl-er': {
        display: 'block',
        width: '100%',
        height: '100%',
        position: 'relative'
      },
      '> *' : {
        display : 'none',
        position: 'absolute',
        width : '100%',
        height : '100%',
        top : 0,
        left : 0,
        objectFit : 'cover'
      },
      '.active' : {
        display : 'block'
      }
    },

    createdCallback : function() {

      // Set up element data
      this.data = {
        speed : 1000,
        list : [],
        current : null,
        cycleTimeout : null,
        playing : false
      }

      // setup
      this.setup(this)
    },

    detachedCallback : function() {
      // this.css.remove()
    },

    setup : function(self) {
      self.data.list  = Array.prototype.slice.call(self.querySelectorAll('*'))
      self.data.speed = self.getAttribute('speed')
    },

    cycle : function(self) {
      
      if (self.data.current) {
        self.data.current.classList.remove('active')
      }

      next(self.data.list, self.data.current).classList.add('active')
      self.data.current = next(self.data.list, self.data.current)


      self.data.cycleTimeout = setTimeout(function() {
        self.cycle(self)
      }, self.data.speed)
    },

    play : function() {
      var self = this
      if (!self.data.playing) {
        self.data.playing = true
        self.cycle(self)
      }
    },

    pause : function() {
      var self = this
      clearTimeout(self.data.cycleTimeout)
      self.data.playing = false
    },

    onEvents : function(self) {
      self.addEventListener('mouseenter', self.pause)
      self.addEventListener('mouseleave', self.play)
    },

    offEvents : function(self) {
      self.removeEventListener('mouseenter', self.pause)
      self.removeEventListener('mouseleave', self.play)
    },

    /**
     * Public Methods
     */

    start : function() {
      var self = this
      self.play()
      // self.onEvents(self)
    },

    stop : function() {
      var self = this
      self.pause()
      // self.offEvents(self)
    }

  }
)

/**
 * Template Setup
 */
exports.template = function templateGet (self) {
  
  var $container = $('<div/>').html(template)

  var images = helpers.randomPhotosOrTexts(self.images, 3)
  var albums = helpers.filteredAssets(images, 'album_')
  
  _.each(images, function(image, index) {
    $container.find('cycl-er').append('<img src="' + image.path + '">')
    if (index === 0) {
      // if no album in set, set aspect ratio to first image
      if (_.isEmpty(albums)) {
        $container.find('cycl-er')
          .attr('width', image.width)
          .attr('height', image.height)
      } else {
        // otherwise set to square
        $container.find('cycl-er')
          .attr('width', 500)
          .attr('height', 500)
      }
    }
  })

  return $container[0].innerHTML
}