var fs = require('fs')
var path = require('path')
var cheerio = require('cheerio')
var restyle = require('restyle')
var peachfuzz = require('peachfuzz')
var $ = require('cash-dom')
var Velocity = require('velocity-animate')
var elementResizeEvent = require('element-resize-event')
var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')
var brick = fs.readFileSync(path.join(__dirname, 'brick.html'), 'utf8')
var helpers = require('../helpers')

var _ = {
  throttle : require('lodash/function/throttle'),
  random : require('lodash/number/random')
}

restyle.customElement(
  'reflect-or',
  HTMLElement,
  {
    css : {
      'reflect-or': {
        display : 'block',
        overflow : 'hidden'
      },
      '[data-reflector]' : {
        position : 'absolute',
        width : '100%',
        height : '100%',
        background : {
          size : 'cover',
          position : 'center',
          repeat : 'no-repeat'
        }
      },
      '[data-reflector="perspective"]' : {
        perspective : '500px'
      },
      '[data-reflector] > [data-reflector]' : {
        transform : 'rotateY(40deg) scale(0.5) translateX(-20%)'
      }
    },

    createdCallback : function() {

      // Set up element data
      this.data = {
        image : '',
        speed : 5000
      }

      // Set up element
      this.setup(this)
      
    },

    setup : function(self) {

      // Get data from attributes
      self.readAttributes()

      // Populate template
      var html = peachfuzz(brick, self.data)

      // Add html
      self.innerHTML = html

    },

    readAttributes: function() {
      this.data.name  = this.getAttribute('name')
      this.data.image = this.getAttribute('image')
      this.data.speed = this.getAttribute('speed')
    },

    detachedCallback : function() {
      // this.css.remove()
    },

    reflect : function() {
      var self = this
      var angle = _.random(-50, 50) + 'deg'
      var translate = _.random(-30, 30) + '%'
      Velocity($(self).find('[data-reflector="animator"]'), {
        'rotateY' : angle,
        'scale' : 0.5,
        'translateX' : translate
      }, 0)
    },

    onEvents : function(self) {
      self.addEventListener('mouseenter', self.reflect)
      // self.addEventListener('mouseleave', self.reflect)
    },

    offEvents : function(self) {
      self.removeEventListener('mouseenter', self.reflect)
      // self.removeEventListener('mouseleave', self.reflect)
    },

    /**
     * Public Methods
     */

    start : function() {
      var self = this
      self.reflect()
      // self.onEvents(self)
    },

    stop : function() {
      var self = this
      // self.offEvents(self)
      // Velocity($(self).find('[data-reflector="animator"]'), 'stop')
    }

  }
)

/**
 * Template Setup
 */
exports.template = function templateGet (self) {
  
  var $container = $('<div/>').html(template)
  
  var image = helpers.randomPhotosOrTexts(self.images, 1)

  if (image) {
    $container.find('reflect-or')
      .attr('image', image.path)
      .attr('width', image.width)
      .attr('height', image.height)
  }
  
  return $container[0].innerHTML
}