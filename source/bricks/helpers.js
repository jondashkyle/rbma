var _ = {
  shuffle : require('lodash/collection/shuffle'),
  filter : require('lodash/collection/filter'),
  sample : require('lodash/collection/sample'),
  includes: require('lodash/collection/includes'),
  slice : require('lodash/array/slice'),
  random : require('lodash/number/random'),
  isObject : require('lodash/lang/isObject'),
  isEmpty : require('lodash/lang/isEmpty'),
}

/**
 * Returns random assets
 */
var randomAssets = function(images, count) {
  count = count || _.random(1, 5)
  return count > 1 ? _.slice(_.shuffle(images), 0, count) : _.sample(images)
}

/**
 * Returns filtered assets
 */
var filteredAssets = function(images, filter, negate) {
  return _.filter(images, function(image) {
    return !negate ? _.includes(image.name, filter) : !_.includes(image.name, filter)
  })
}

/**
 * Returns filtered assets randomized
 */
var randomFilteredAssets = function(images, count, filter, negate) {
  return randomAssets(filteredAssets(images, filter, negate), count)
}

/**
 * Returns random photo assets (filename does not include `text_`)
 */
var randomPhotos = function(images, count) {
  return randomAssets(filteredAssets(images, 'text_', true), count)
}

/**
 * Returns random text assets (filename includes `text_`)
 */
var randomTexts = function(images, count) {
  return randomAssets(filteredAssets(images, 'text_'), count)
}

/**
 * Returns random photos or text assets
 */
var randomPhotosOrTexts = function(images, count) {
  var selected = null
  var isPhotos = _.sample([true, true, true, true, false])
  if (isPhotos) {
    selected = randomPhotos(images, count)
    if (_.isEmpty(selected)) selected = randomTexts(images, count)
  } else {
    selected = randomTexts(images, count)
    if (_.isEmpty(selected)) selected = randomPhotos(images, count)
  }
  return selected
}

/**
 * Public
 */
module.exports = {
  randomAssets : randomAssets,
  filteredAssets : filteredAssets,
  randomFilteredAssets : randomFilteredAssets,
  randomPhotos : randomPhotos,
  randomTexts : randomTexts,
  randomPhotosOrTexts : randomPhotosOrTexts
}