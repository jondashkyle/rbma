var $ = require('cash-dom')
var fs = require('fs')
var path = require('path')
var restyle = require('restyle')
var peachfuzz = require('peachfuzz')
var cheerio = require('cheerio')
var Velocity = require('velocity-animate')
var elementResizeEvent = require('element-resize-event')
var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')
var brick = fs.readFileSync(path.join(__dirname, 'brick.html'), 'utf8')
var helpers = require('../helpers')

var _ = {
  throttle : require('lodash/function/throttle'),
  each : require('lodash/collection/each')
}

restyle.customElement(
  'zoom-er',
  HTMLElement,
  {
    css : {
      'zoom-er': {
        display: 'block',
        width: '100%',
        height: '100%',
        position: 'relative'
      },
      '[data-zoom]' : {
        position : 'relative',
        width : '100%',
        height : '100%',
        overflow : 'hidden',
        boxSizing : 'border-box'
      },
      '[data-zoom] [data-img]' : {
        position: 'absolute',
        width : '100%',
        height : '100%',
        top : 0,
        left : 0,
        objectFit : 'cover',
        background : {
          size : 'cover',
          position : 'center',
          repeat : 'no-repeat'
        }
      }
    },

    createdCallback : function() {

      // Set up element data
      this.data = {
        image : '',
        speed : 5000,
        playing : false
      }

      // Set up element
      this.setup(this)
      
    },

    setup : function(self) {

      // Get data from attributes
      self.readAttributes()

      // Populate template
      var html = peachfuzz(brick, self.data)

      // Add html
      self.innerHTML = html

      // Set padding
      setTimeout(function() { self.setPadding(self) }, 0) // <- nice
      elementResizeEvent(self, _.throttle(function() {
        self.setPadding(self)
      }, 50))

    },

    readAttributes: function() {
      this.data.name  = this.getAttribute('name')
      this.data.image = this.getAttribute('image')
      this.data.speed = this.getAttribute('speed')
    },

    detachedCallback : function() {
      // this.css.remove()
    },

    setPadding : function(self) {
      _.each(self.querySelectorAll('[data-zoom]'), function(element) {
        var base = self.offsetHeight > self.offsetWidth ? self.offsetWidth : self.offsetHeight
        var padding = base * 0.15
        element.style.padding = padding + 'px'
      })
    },

    zoom : function() {
      var self = this
      Velocity(self.querySelectorAll('[data-img]'), {
        scale : [1.2, 1]
      }, {
        duration : self.data.speed,
        easing : 'linear',
        complete : function() {
          self.zoom()
        }
      })
    },

    play : function() {
      var self = this
      if (!self.data.playing) {
        self.zoom()
        self.data.playing = true
      }
    },

    pause : function() {
      var self = this
      Velocity(self.querySelectorAll('[data-img]'), 'stop')
      self.data.playing = false
    },

    onEvents : function(self) {
      self.addEventListener('mouseenter', self.pause)
      self.addEventListener('mouseleave', self.play)
    },

    offEvents : function(self) {
      self.removeEventListener('mouseenter', self.pause)
      self.removeEventListener('mouseleave', self.play)
    },

    /**
     * Public Methods
     */

    start : function() {
      var self = this
      self.play()
      setTimeout(function() { self.setPadding(self) }, 0)
      // self.onEvents(self)
    },

    stop : function() {
      var self = this
      self.pause()
      // self.offEvents(self)
    }

  }
)

/**
 * Template Setup
 */
exports.template = function templateGet (self) {
  
  var $container = $('<div/>').html(template)
  
  var image = helpers.randomAssets(self.images, 1)
  
  $container.find('zoom-er')
    .attr('image', image.path)
    .attr('width', image.width)
    .attr('height', image.height)
  
  return $container[0].innerHTML
}