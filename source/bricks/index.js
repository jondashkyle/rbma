var bricks = {
  cycle: require('./cycler'),
  zoomer: require('./zoomer'),
  reflector: require('./reflector'),
  fader: require('./fader'),
  partitioner: require('./partitioner')
}

function random (content) {
  var temp_key, keys = []
  for(temp_key in bricks) {
   if(bricks.hasOwnProperty(temp_key)) {
     keys.push(temp_key)
   }
  }
  return bricks[keys[Math.floor(Math.random() * keys.length)]]
}

module.exports = {
  bricks: bricks,
  random: random
}