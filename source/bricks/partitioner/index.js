var fs = require('fs')
var path = require('path')
var restyle = require('restyle')
var peachfuzz = require('peachfuzz')
var $ = require('cash-dom')
var Velocity = require('velocity-animate')
var elementResizeEvent = require('element-resize-event')
var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')
var brick = fs.readFileSync(path.join(__dirname, 'brick.html'), 'utf8')
var helpers = require('../helpers')

var _ = {
  throttle : require('lodash/function/throttle'),
  each : require('lodash/collection/each'),
  sample : require('lodash/collection/sample')
}

restyle.customElement(
  'partition-er',
  HTMLElement,
  {
    css : {
      'partition-er': {
        display: 'block'
      },
      'div' : {
        position : 'relative',
        width : '50%',
        height : '50%',
        'float' : 'left',
        background : {
          size : 'cover',
          position : 'center',
          repeat : 'no-repeat'
        }
      },
      '[data-unit]' : {
        position : 'absolute'
      },
      '[data-unit]:first-child' : {
        top : 0,
        left : 0
      },
      '[data-unit]:last-child' : {
        bottom : 0,
        right : 0
      },
      '[data-unit].invert:first-child' : {
        left: 'auto',
        right : 0
      },
      '[data-unit].invert:last-child' : {
        left : 0,
        right: 'auto'
      }
    },

    createdCallback : function() {

      // Set up element data
      this.data = {
        image : '',
        speed : 5000
      }

      // Set up element
      this.setup(this)
      
    },

    setup : function(self) {

      // Get data from attributes
      self.readAttributes()

      // Populate template
      var html = peachfuzz(brick, self.data)

      for (var i = 0; i < 2; i++) {
        html += html
      }

      // Add html
      self.innerHTML = html

      // invert this thing?
      if (_.sample([true, false])) {
        $(self).find('[data-unit]').addClass('invert')
      }

    },

    readAttributes: function() {
      this.data.name  = this.getAttribute('name')
      this.data.image = this.getAttribute('image')
      this.data.speed = this.getAttribute('speed')
    },

    switchEm : function() {
      $(this).find('[data-unit]:first-child').css({
        transform : 'translateX(100%)'
      })
      $(this).find('[data-unit]:last-child').css({
        transform : 'translateX(-100%)'
      })
    },

    revertEm : function() {
      $(this).find('[data-unit]:first-child').css({
        transform : 'translateX(0)'
      })
      $(this).find('[data-unit]:last-child').css({
        transform : 'translateX(0)'
      })
    },

    onEvents : function(self) {
      self.addEventListener('mouseenter', self.switchEm)
      self.addEventListener('mouseleave', self.revertEm)
    },

    offEvents : function(self) {
      self.removeEventListener('mouseenter', self.switchEm)
      self.removeEventListener('mouseleave', self.revertEm)
    },

    /**
     * Public Methods
     */

    start : function() {
      var self = this
      // self.onEvents(self)
    },

    stop : function() {
      var self = this
      // self.offEvents(self)
    }

  }
)

/**
 * Template Setup
 */
exports.template = function templateGet (self) {
  
  var $container = $('<div/>').html(template)
  
  var image = helpers.randomPhotosOrTexts(self.images, 1)

  if (image) {
    $container.find('partition-er')
      .attr('image', image.path)
      .attr('width', image.width)
      .attr('height', image.height)
  }
  
  return $container[0].innerHTML
}