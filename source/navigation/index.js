var $ = require('cash-dom')
var Handlebars = require('handlebars')
var Hammer = require('hammerjs')
var mobile = require('is-mobile')
var tap = require('tap-event')
var fs = require('fs')
var path = require('path')
var restyle = require('restyle')
var raf = require('raf-loop')
var addWheelListener = require('wheel')
var velocity = require('velocity-animate')
var prefix = require('vendor-prefix')
var template = Handlebars.compile(fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8'))

var _ = {
  each: require('lodash/collection/each'),
  shuffle: require('lodash/collection/shuffle')
}

exports.setup = function setup (content) {
  if (typeof content !== 'object') {
    return false
  }

  var data = {
    active: false,
    position: -100,
    force: 0.95,
    delta: 0,
    width: 0
  }

  var loop = raf(frame)
  var vendor = prefix.dash('transform')

  $('body').append(template(content))

  var hammertime = new Hammer(document.querySelector('[data-navigation]'))

  function show () {
    if (! data.active) {
      velocity($('[data-navigation]'), {
        translateY: ['0px', '158px']
      }, {
        duration: 500,
        easing: 'easeOutQuint',
        display: 'block'
      })
      data.active = true
      loop.start()
    }

    if (mobile(navigator.userAgent)) {
      $('.page-hide-nav').css('display', 'block')
    }
  }

  function hide () {
    if (data.active) {
      velocity($('[data-navigation]'), {
        translateY: ['158px', '0px']
      }, {
        duration: 250,
        easing: 'easeOutQuint',
        complete: function() {
          loop.stop()
        },
        display: 'none'
      })

      if (mobile(navigator.userAgent)) {
        $('.page-hide-nav').css('display', 'none')
      }

      data.active = false
    }
  }

  function toggle () {
    if (data.active) {
      hide()
    } else {
      show()
    }
  }

  function reset () {
    $('[data-nav-logo]').removeClass('active')
  }

  function setActive (page) {
    if (page) {
      reset()
      $('[data-nav-logo="' + page + '"]').addClass('active')
    }
  }

  function resize () {
    data.width = 300 * content.length
    $('.navigation-logos-group').css('width', data.width + 'px')
    $('.navigation-logos').css('width', data.width * 2 + 'px')
  }

  function frame () {
    data.position -= 0.5

    if (Math.abs(data.delta) > 0.01) {
      data.delta = data.delta * data.force
    } else {
      data.delta = 0
    }

    if (data.position > 0) {
      data.position = data.width * -1
    } else if (data.position < data.width * -1) {
      data.position = 0
    }

    $logos.css(vendor, 'translateX(' + (data.position += data.delta) + 'px) translateZ(0)')
  }

  function scroll (delta) {
    if (delta > 50) {
      delta = 50
    } else if (delta < -50) {
      delta = -50
    }

    if (Math.abs(delta) > Math.abs(data.delta)) {
      data.delta = delta * -1
    }
  }

  hammertime.on('panleft panright', function(e) {
    scroll(e.deltaX * -1 / 10 || e.deltaY * -1 / 10)
    e.gesture.preventDefault()
  })

  $('body')
    .on('click', '[data-navigation-show]', show)
    .on('click', '[data-navigation-hide]', hide)
    .on('touchstart', '[data-navigation-hide]', hide)
    .on('click', '[data-navigation-toggle]', toggle)

  // $('[data-navigation-hover-show]')[0].addEventListener('mouseenter', show)

  resize()

  var $logos = $('.navigation-logos')

  addWheelListener(document.querySelector('[data-navigation]'), function (e) {
    scroll(e.deltaX || e.deltaY)
    e.preventDefault()
    window.scrollLeft = 0
    return false
  }, false)

  $logos[0].addEventListener('mouseleave', hide, false)
  
  return {
    show: show,
    hide: hide,
    toggle: toggle,
    setActive: setActive,
    reset: reset
  }
}