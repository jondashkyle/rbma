var scrolltop = require('scrolltop')
var animate = require('animate')
var _ = {
 throttle : require('lodash/function/throttle')
}

module.exports = function (rate) {

  rate = rate || 15

  var _data = {
    loop : null,
    scrolling : false,
    scrollTimer : null,
    scrolltop : scrolltop(),
    scrolltopLast : 0
  }

  _data.loop = animate(scroll, 1000 / rate)
  _data.loop.pause() // Don't autoplay
 
  function scroll () {
    var pos = _data.scrolltop + 1
    window.scrollTo(0, pos)
  }

  function play () {
    if (!_data.scrolling) {
      _data.scrolling = true
      _data.loop.resume()
    }
  }

  function pause () {
    if (_data.scrolling) {
      _data.scrolling = false
      _data.loop.pause()
    }
  }
  
  var scrollHandler = _.throttle(function () {

    _data.scrolltop = scrolltop()

    // if user is scrolling, stop autoscroll
    if (_data.scrolltop <= 0 || Math.abs(_data.scrolltop - _data.scrolltopLast) > 5) {
      pause()
    } else {
      play()
    }

    _data.scrolltopLast = _data.scrolltop

    clearInterval(_data.scrollTimer)
    _data.scrollTimer = setTimeout(play, 100)

  }, 10)


  /**
   * Public
   */

  function init () {
    window.addEventListener('scroll', scrollHandler)
    play()
  }

  function destroy () {
    window.removeEventListener('scroll', scrollHandler)
    clearInterval(_data.scrollTimer)
    _data.scrolltop = 0
    _data.scrolltopLast = 0
    pause()
  }

  return {
    'init' : init,
    'destroy' : destroy
  }

}