var $ = require('cash-dom')
var prefix = require('vendor-prefix')
var fs = require('fs')
var path = require('path')
var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')

/**
 * Setup
 */
function setup (content) {

  $('body').append(template)

  $('html').on('click', remove)

  function remove() {
    $('html').off('click', remove)
    $('body').removeClass('intro')
    $('.navigation-triggers').css('display', 'block')
    if (document.querySelectorAll('[data-header]').length) {
      document.body.removeChild(document.querySelector('[data-header]'))
    }
  }
  
  return {
    'remove' : remove
  }
}

/**
 * Public methods
 */
exports.setup = setup