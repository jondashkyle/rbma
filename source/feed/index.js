var $ = require('cash-dom')
var cheerio = require('cheerio')
var fs = require('fs')
var path = require('path')
var mobile = require('is-mobile')
var scrollTop = require('scrolltop')
var scrollMonitor = require('scrollmonitor')
var autoscroll = require('../modules/autoscroll')(30)
var EventEmitter = require('events').EventEmitter
var $template = cheerio.load(fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8'))
var $brick = cheerio.load(fs.readFileSync(path.join(__dirname, 'brick.html'), 'utf8'))

var scroll

var options = {
  bricks: null,
  content: null
}

var _ = {
  each: require('lodash/collection/each'),
  shuffle: require('lodash/collection/shuffle'),
  extend: require('lodash/object/extend'),
  random: require('lodash/number/random'),
  toArray: require('lodash/lang/toArray')
}

function getDocHeight() {
  var D = document
  return Math.max(
    D.body.scrollHeight, D.documentElement.scrollHeight,
    D.body.offsetHeight, D.documentElement.offsetHeight,
    D.body.clientHeight, D.documentElement.clientHeight
  )
}

/**
 * Setup
 * @param  {obj} content Pages
 * @param  {obj} bricks  Templates
 */
exports.setup = function setup (opts) {
  if (! opts.content || ! opts.bricks) {
    console.warn('Please pass data')
  }

  options = _.extend(options, opts)

  var events = new EventEmitter()
  var watchers = [ ]

  function on (ev, cb) {
    events.on(ev, cb)
    return this
  }

  function off (ev, cb) {
    events.removeListener(ev, cb)
    return this
  }

  function scroll () {
    if (scrollTop() >= getDocHeight() - (window.innerHeight * 1.5)) {
      events.emit('end')
    }
  }

  function labelShow (label) {
    $('[data-current-label] span').text(label)
    $('[data-current-label]').css('display', 'block')
  }

  function labelHide () {
    $('[data-current-label]').css('display', 'none')
  }

  function paginate(container) {
    if (! container) {
      container = options.container
    }

    _.each(_.shuffle(options.content), function(self) {
      var brick = options.bricks.random()
      var $block = $('<div class="block"/>')
      var $brick = $(brick.template(self))
      var $padder = $('<div class="padder"/>')
      var elementWatcher = scrollMonitor.create($brick)
      
      // Watch elements
      watchers.push(elementWatcher)

      // Append Brick
      $brick.appendTo($block)
      $padder.appendTo($block)

      // Set padding
      var padding = _.random(75, 125) + '%'
      if ($brick.attr('width') && $brick.attr('height')) {
        padding = $brick.attr('height') / $brick.attr('width') * 100 + '%'
      }

      $padder.css({ 'paddingTop' : padding })

      // Set width
      var zoom  = _.random(2.1, 4)
      var width = _.random(10, 13) * zoom + '%'
      if ( parseFloat(padding) > 90 ) {
        width = _.random(12, 18) * zoom + '%'
      }

      $block.css({'width' : width})

      $brick.attr('data-label', self.name)
      $brick.attr('data-page', '/' + self.path)
       
      elementWatcher.enterViewport(function () {
        if (typeof $brick[0].start === 'function') {
          $brick.css('display', 'block')
          $brick[0].start()
        }
      })

      elementWatcher.exitViewport(function () {
        if (typeof $brick[0].stop === 'function') {
          $brick.css('display', 'none')
          $brick[0].stop()
        }
      })

      if (! mobile(navigator.userAgent)) {
        $brick[0].addEventListener('mouseenter', function () {
          var label = this.getAttribute('data-label')
          labelShow(label)
          events.emit('brick:mouseenter', label)
        }, false)

        $brick[0].addEventListener('mouseleave', function () {
          labelHide()
          events.emit('brick:mouseleave')
        }, false)
      }

      container.append($block)
    })
  
    events.emit('paginate')
  }

  function load () {
    events.emit('load')
    window.addEventListener('scroll', scroll, false)
    labelHide()
    autoscroll.init()

    // to fix safari being a butt
    _.each(_.toArray($('.block')).slice(0, 8), function(block) {
      var $brick = $(block).children().first()
      $brick.css('display', 'block')
      setTimeout(function() { $brick[0].start() }, 100)
    })

  }

  function unload () {
    events.emit('unload')
    window.removeEventListener('scroll', scroll, false)
    labelHide()
    autoscroll.destroy()
  }

  function reset () {
    _.each(watchers, function (self) {
      self.destroy()
    })

    watchers = [ ]

    $('[data-feed]').empty()
  }

  $('body').append($template.html())
  $('body').append('<div class="current-label" data-current-label><span></span></div>')

  return {
    load: load,
    unload: unload,
    reset: reset,
    on: on,
    off: off,
    paginate: paginate
  }
}