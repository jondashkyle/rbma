var $ = require('cash-dom')
var mobile = require('is-mobile')
var tap = require('tap-event')
var attachFastClick = require('fastclick')
var routebeer = require('routebeer')
var MediaSize = require('mediasize')
var scrollTo = require('scroll-to')
var analytics = require('ga-browser')()
var inIframe = require('./modules/inIframe')

try {
  Typekit.load({
    active : function() {
      $('body').trigger('fontready')
    }
  })
} catch (e) { }

/**
 * Configuration
 */
var content = require('./content')
var bricks = require('./bricks')

/**
 * Constructors
 */
var Header = require('./header')
var Nav = require('./navigation')
var About = require('./about')
var Feed = require('./feed')
var Pages = require('./pages')

/**
 * Instances
 */
var header = Header.setup(content)
var nav = Nav.setup(content)
var about = About.setup(content)
var pages = Pages.setup(content)

var feed = Feed.setup({
  bricks: bricks,
  content: content
})

var transition = require('./pages/transition')

feed.on('end', function () {
  feed.paginate($('[data-feed]'))
})

feed.on('load', function () {
  $('[data-feed]').css('display', 'block')
})

feed.on('unload', function () {
  $('[data-feed]').css('display', 'none')
})

pages.on('end:up', function () {
  nav.hide()
})

pages.on('end', function () {
  nav.show()
})

/**
 * Routes
 */

var visited = false

routebeer.init({
  event: 'popstate',
  always : function always (data) {
    if (data.name !== 'feed') {
      scrollTo(0, 0, {
        duration: 1
      })
    }

    if (!visited) {
      $('[data-preloader]').remove()
    }

    nav.hide()
    about.hide()

    webtrekk()
    analytics('send', 'pageview', {
      page: window.location.pathname,
      title: data.name
    })
  }
})

routebeer.add({
  name: 'feed',
  pattern: '/',
  load: function () {
    $('body').removeClass('subpage')

    feed.paginate($('[data-feed]'))

    feed.load()
    nav.reset()

    if (!visited) {
      $('[data-header]').css('display', 'block')
      $('body').addClass('intro') 
    } else {
      header.remove()
    }
  },
  unload: function () {
    visited = true
    $('[data-header]').css('display', 'none')
    feed.unload()
    feed.reset()
  }
})

routebeer.add({
  name: 'pages',
  pattern: '/:page',
  load: function (data) {
    if (!visited) {
      $('.navigation-triggers').css('display', 'block')
    }
    visited = true
    $('body').addClass('subpage')
    pages.load(data.page)
    nav.setActive(data.page)
  },
  unload: function (data) {
    pages.unload(data.page)
  }
})

routebeer.navigate()

$('body').on('click', '[data-page]', function pageClick (event) {
  history.pushState({ }, '', $(this).attr('data-page'))
  transition.wipe($(this).attr('data-page'), content, routebeer.navigate)
  $('[data-current-label]').css('display', 'none')
})

$('body').on('touchstart', '[data-page]', tap(function (e) {
  history.pushState({ }, '', $(this).attr('data-page'))
  transition.wipe($(this).attr('data-page'), content, routebeer.navigate)
  $('[data-current-label]').css('display', 'none')
}))

$('a:not([target])').on('click', function pageClick (event) {
  event.preventDefault()
  history.pushState({ }, '', $(this).attr('href'))
  routebeer.navigate()
})

/**
 * iframe Check
 */
// if (!inIframe()) {
//   $('html').removeAttr('data-iframe')
//   $('.nav-dummy').remove()
// }

/**
 * Mobile
 */
if (mobile(navigator.userAgent)) {
  $('html').addClass('is-mobile')
}

attachFastClick(document.body)

/**
 * Google Analytics
 */
analytics('create', 'UA-21347469-1', 'auto')

function webtrekk () {
  edWrapper.push( [ 'trackPageView' , false , {1:'IntMS' , 2:'IntMS - int' , 3:'IntMS - int - labels.redbullmusicacademy.com'} , true ] );
  edWrapper.push( [ 'trackPageVar' , 'Language' , 'en'] );
  edWrapper.push( [ 'trackPageVar' , 'Domain incl. Subdomain' , window.location.hostname] );
  edWrapper.push( [ 'trackPageVar' , 'Type' , 'IntMS'] );
  edWrapper.push(['submit']);   
}