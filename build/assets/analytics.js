var edWrapperNameSpace = {};

edWrapperNameSpace.aGTM = [
  "GTM-LVJ5",   // hq
  "GTM-7W7M",   // hq
  "GTM-HPZK",   // hq
  "GTM-TDJ7W8"  // property
];    

var edWrapper = [];

edWrapperNameSpace.sVers = -1; 
edWrapperNameSpace.aInitialInnerWrapper = [["downloads.redbull.com/webtrekk/innereWrapper/gtm/innerWrapper_gtm.js",false]];
edWrapperNameSpace.sWrapperSrc = ["downloads.redbull.com/webtrekk/edAnalyticsWrapper.js","downloads.redbull.com/webtrekk/confWrapper.js"];

(function (sVarName) {
  window.edWrapperNameSpace.sEdWrapperObjectName = sVarName; window[sVarName] = window[sVarName] || [];
  var script = document.createElement('script'); script.type = 'text/javascript'; script.asyn = false; 
  script.src = ('https:' == document.location.protocol ? 'https://' : 'http://')+edWrapperNameSpace.sWrapperSrc[0] + "?v=" + edWrapperNameSpace.sVers;
  var script2 = document.getElementsByTagName('script')[0]; script2.parentNode.insertBefore(script, script2);
})("edWrapper");