var fs = require('fs')
var path = require('path')
var marked = require('marked')
var cheerio = require('cheerio')
var stripIndent = require('strip-indent')
var directoryTree = require('directory-tree').directoryTree
var beautify = require('js-beautify').js_beautify
var sizeOf = require('image-size')

var _ = {
  has: require('lodash/object/has'),
  forEach: require('lodash/collection/foreach'),
  where: require('lodash/collection/where'),
  pluck: require('lodash/collection/pluck'),
  filter: require('lodash/collection/filter'),
  includes: require('lodash/collection/includes'),
}

var parent = '/build/content'

var content = directoryTree(path.join(__dirname, parent), [
  '.md', '.jpg', '.jpeg', '.png', '.svg', '.html', '.less', '.js'
])

marked.setOptions({
  breaks: true,
  sanitize: true,
  smartLists: true,
  smartypants: true
})

if (content) {
  content = content.children
  _.forEach(content, format)
  setTimeout(function() {
    console.log('Finished!')
    content = beautify(JSON.stringify(content), { indent_size: 2 })
    fs.writeFile(path.join(__dirname, 'source/content.json'), content)
  }, 1000)
}

/**
 * Format the entry by adding the content markdown
 */
function format (self) {
  if (_.has(self, 'children')) {
    var md = _.pluck(_.where(self.children, { name: 'content.md' }), 'path')[0]

    // Content
    if (md) {
      var text = fs.readFileSync(path.join(__dirname, parent, md), 'utf8')
      self.content = formatText(text, self)
    }

    // Name
    self.name = prettifyString(self.name)

    // Images
    self.images = _.filter(self.children, function(item) {
      if (imageCheck(item.name)) {
        var file = path.join(__dirname, parent, item.path)

        var dimensions = sizeOf(file)
        item.width = dimensions.width
        item.height = dimensions.height

        item.path = 'content/' + item.path

        var name = item.name.replace(/\.[^/.]+$/, '')
        var capFile = _.pluck(_.where(self.children, { name: name + '.md' }), 'path')[0]
        if (capFile) {
          var capText = fs.readFileSync(path.join(__dirname, parent, capFile), 'utf8')
          item.caption = marked(capText)
        }

        return item
      }
    })

    // Assets
    self.assets = _.pluck(_.where(self.children, { 'name' : 'assets', 'type' : 'directory' }), 'children')

  }
}

/**
 * Check to see if a file is an image
 */
function imageCheck (name) {
  if (_.includes(name, '.jpg')) {
    return true
  } else if(_.includes(name, '.jpeg')) {
    return true
  } else if (_.includes(name, '.gif')) {
    return true
  } else if (_.includes(name, '.png')) {
    return true
  }
}

/**
 * Format Text
 */
function formatText (text, self) {
  var $text

  // Single row
  text = text.replace(/\{{row\}}/g, '<div class="row">')
  text = text.replace(/\{{\/row\}}/g, '</div>')

  // Row options
  text = text.replace(/\{{row ([\s\S](?:(?!}}).)*)\}}/g, '<div class="row $1">')
  text = text.replace(/\{{\/row\}}/g, '</div>')

  // Text
  text = text.replace(/\{{text\}}/g, '<div class="text">')
  text = text.replace(/\{{\/text\}}/g, '</div>')

  // Column
  text = text.replace(/\{{col ([\s\S](?:(?!}}).)*)\}}/g, '<div class="col $1">')
  text = text.replace(/\{{\/col\}}/g, '</div>')

  $text = cheerio.load(text)

  // Format text blocks with markdown
  $text.root().find('.text').each(function() {
    var md = marked(stripIndent($text(this).html()))
    $text(this).html(md)
  })

  // Images
  $text = $text.html().replace(/\{{image ([\s\S](?:(?!}}).)*)\}}/g, '<div><img data-src="content/' + self.path + '/$1"></div>')
  $text = $text.replace(/\{{youtube ([\s\S](?:(?!}}).)*)\}}/g, '<div class="youtube" data-youtube="$1"></div>')

  // Text Units
  $text = $text.replace(/\{{unit\}}/g, '<span class="unit">')
  $text = $text.replace(/\{{\/unit\}}/g, '</span>')

  return $text
}

/**
 * Replace hyphens with spaces and capitalize
 */
function prettifyString(str) {
  return str.replace(/(-|^)([^-]?)/g, function(i, prep, letter) {
    return (prep && ' ') + letter.toUpperCase()
  })
}